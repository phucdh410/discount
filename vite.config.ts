import react from '@vitejs/plugin-react-swc';
import { fileURLToPath, URL } from 'node:url';
import { defineConfig, loadEnv } from 'vite';
import { VitePWA } from 'vite-plugin-pwa';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const _env = loadEnv(mode, process.cwd());

  return {
    plugins: [
      react(),
      VitePWA({
        registerType: 'autoUpdate',

        manifest: {
          screenshots: [
            {
              src: 'shit.jpg',
              sizes: '500x500',
              type: 'image/jpg',
              form_factor: 'wide',
              label: 'Wonder Widgets',
              platform: 'windows',
            },
            {
              src: 'shit.jpg',
              sizes: '500x500',
              type: 'image/jpg',
              form_factor: 'narrow',
              label: 'Wonder Widgets',
              platform: 'android',
            },
            {
              src: 'shit.jpg',
              sizes: '500x500',
              type: 'image/jpg',
              form_factor: 'narrow',
              label: 'Wonder Widgets',
              platform: 'ios',
            },
          ],
          theme_color: '#ffffff',
          background_color: '#ffffff',
          icons: [
            {
              purpose: 'maskable',
              sizes: '512x512',
              src: 'icon512_maskable.png',
              type: 'image/png',
            },
            {
              purpose: 'any',
              sizes: '512x512',
              src: 'icon512_rounded.png',
              type: 'image/png',
            },
          ],
          orientation: 'any',
          display: 'standalone',
          dir: 'ltr',
          lang: 'vi',
          name: 'DHPhuc',
          short_name: 'DHP',
        },
      }),
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
      },
    },
    server: {
      proxy: {
        '/api': _env.VITE_API_URL,
      },
    },
    build: {
      assetsDir: 'static',
      rollupOptions: {
        output: {
          chunkFileNames: 'static/[hash].chunk.js',
          assetFileNames: 'static/[hash].chunk.[ext]',
        },
      },
    },
  };
});
