import {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from 'react';
import { Button, Dialog, Flex, Text, TextField } from '@radix-ui/themes';

import { IFood } from '@/types';
import { onSelectAll } from '@/utils/funcs';

export interface IModalContentRef {
  clear: () => void;
}
export interface IModalContentProps {
  onSave: (newFood: IFood) => void;
}

export const ModalContent = forwardRef<IModalContentRef, IModalContentProps>(
  ({ onSave }, ref) => {
    //#region Data
    const submitRef = useRef<HTMLButtonElement | null>(null);

    const [name, setName] = useState<string>('');
    const [price, setPrice] = useState<'' | number>('');
    const [amount, setAmount] = useState<number>(1);
    const [discountPrice, setDiscountPrice] = useState<'' | number>('');
    //#endregion

    //#region Event
    const onNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setName(event.target.value);
    };

    const onPriceChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setPrice(Number(event.target.value));
    };

    const onAmountChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setAmount(Number(event.target.value));
    };

    const onDiscountPriceChange = (
      event: React.ChangeEvent<HTMLInputElement>,
    ) => {
      setDiscountPrice(Number(event.target.value));
    };

    const onClear = () => {
      setName('');
      setPrice('');
      setAmount(1);
      setDiscountPrice('');
    };

    const handleSave = () => {
      const uid = Math.random() * 10000000 + new Date().toDateString();
      const food: IFood = {
        uid,
        name: name.trim(),
        price,
        amount,
        discount_price: discountPrice,
      };
      onSave(food);
    };

    const onKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
      if (event.code === 'Enter') {
        submitRef.current?.click();
      }
    };
    //#endregion

    useImperativeHandle(ref, () => ({
      clear: () => onClear(),
    }));

    useEffect(() => {
      if (discountPrice) {
        setAmount(1);
      }
    }, [discountPrice]);

    //#region Render
    return (
      <Dialog.Content>
        <Dialog.Title>Thêm món ăn</Dialog.Title>
        <Flex direction="column" gap="3">
          <label>
            <Text as="div" size="2" mb="1" weight="medium">
              Tên món ăn
            </Text>
            <TextField.Input
              radius="large"
              value={name}
              onChange={onNameChange}
              onKeyDown={onKeyPress}
              placeholder="Nhập tên món ăn"
            />
          </label>
          <label>
            <Text as="div" size="2" mb="1" weight="medium">
              Đơn giá
            </Text>
            <TextField.Input
              radius="large"
              type="number"
              value={price}
              onChange={onPriceChange}
              onClick={onSelectAll}
              onKeyDown={onKeyPress}
              placeholder="Nhập đơn giá"
            />
          </label>
          <label>
            <Text as="div" size="2" mb="1" weight="medium">
              Giá khuyến mãi (nếu có)
            </Text>
            <TextField.Input
              radius="large"
              type="number"
              value={discountPrice}
              onChange={onDiscountPriceChange}
              onClick={onSelectAll}
              onKeyDown={onKeyPress}
              placeholder="Nhập giá khuyến mãi"
            />
          </label>
          <label>
            <Text as="div" size="2" mb="1" weight="medium">
              Số lượng
            </Text>
            <TextField.Input
              radius="large"
              type="number"
              disabled={!!discountPrice}
              value={amount}
              onChange={onAmountChange}
              onClick={onSelectAll}
              onKeyDown={onKeyPress}
              placeholder="Nhập số lượng"
            />
          </label>
        </Flex>

        <Flex gap="3" mt="4" justify="end">
          <Dialog.Close>
            <Button variant="soft" color="gray" onClick={onClear}>
              Hủy
            </Button>
          </Dialog.Close>
          <Dialog.Close>
            <Button ref={submitRef} onClick={handleSave}>
              Thêm
            </Button>
          </Dialog.Close>
        </Flex>
      </Dialog.Content>
    );
    //#endregion
  },
);
