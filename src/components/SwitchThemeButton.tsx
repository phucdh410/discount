import { useEffect, useState } from 'react';
import { MoonIcon, SunIcon } from '@radix-ui/react-icons';
import { Flex, IconButton } from '@radix-ui/themes';

const darkModeMediaQuery = window.matchMedia('(prefers-color-scheme: dark)');

const prefersDarkMode = window.matchMedia(
  '(prefers-color-scheme: dark)',
).matches;

export const SwitchThemeButton = () => {
  //#region Data
  const [themeMode, setThemeMode] = useState<'light' | 'dark'>(
    darkModeMediaQuery.matches ? 'dark' : 'light',
  );
  //#endregion

  //#region Event
  const onSwitchToDark = () => {
    setThemeMode('dark');
  };

  const onSwitchToLight = () => {
    setThemeMode('light');
  };
  //#endregion

  useEffect(() => {
    prefersDarkMode && setThemeMode('dark');

    const handleChange = () =>
      setThemeMode(darkModeMediaQuery.matches ? 'dark' : 'light');

    darkModeMediaQuery.addEventListener('change', handleChange);

    return () => {
      darkModeMediaQuery.removeEventListener('change', handleChange);
    };
  }, []);

  useEffect(() => {
    if (themeMode === 'light') {
      document.documentElement.classList.add('light');
      document.documentElement.classList.remove('dark');
    } else {
      document.documentElement.classList.add('dark');
      document.documentElement.classList.remove('light');
    }
    // localStorage.setItem('theme', themeMode);
  }, [themeMode]);

  //#region Render
  return (
    <Flex
      gap="2"
      align="center"
      justify="center"
      position="absolute"
      bottom="0"
      right="0"
      mr="5"
      mb="5"
    >
      <IconButton
        disabled={themeMode === 'light'}
        onClick={onSwitchToLight}
        style={{ cursor: 'pointer' }}
      >
        <SunIcon height="16" width="16" />
      </IconButton>
      <IconButton
        disabled={themeMode === 'dark'}
        onClick={onSwitchToDark}
        style={{ cursor: 'pointer' }}
      >
        <MoonIcon height="16" width="16" />
      </IconButton>
    </Flex>
  );
  //#endregion
};
