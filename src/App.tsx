import { useMemo, useRef, useState } from 'react';
import { Cross2Icon, PlusIcon } from '@radix-ui/react-icons';
import {
  Box,
  Button,
  Card,
  Dialog,
  Flex,
  IconButton,
  Separator,
  Text,
  TextField,
  Theme,
} from '@radix-ui/themes';

import { formatMoney, onSelectAll } from './utils/funcs';
import {
  IModalContentRef,
  ModalContent,
  SwitchThemeButton,
} from './components';
import { IFood } from './types';

import './styles/index.scss';
import '@radix-ui/themes/styles.css';

// const MOCK: IFood[] = [
//   {
//     uid: '12312d',
//     name: 'Gà mắm tỏi',
//     amount: 2,
//     price: 41400,
//     discount_price: '',
//   },
//   {
//     uid: '1231',
//     name: 'Gà xíu',
//     amount: 2,
//     price: 41400,
//     discount_price: '',
//   },
//   {
//     uid: '476566575',
//     name: 'Gà heo giòn',
//     amount: 1,
//     price: 41400,
//     discount_price: '',
//   },
//   {
//     uid: '14rr',
//     name: 'Gà xá xíu',
//     amount: 1,
//     price: 41400,
//     discount_price: 23000,
//   },
// ];

const PHI_GIAO_HANG = 0;
const TIEN_GIAM_GIA = 0;

function App() {
  //#region Data
  const orderRef = useRef<IModalContentRef | null>(null);

  const [foodList, setFoodList] = useState<IFood[]>([]);
  const [shipFee, setShipFee] = useState<number>(PHI_GIAO_HANG | 0);
  const [discount, setDiscount] = useState<number>(TIEN_GIAM_GIA | 0);

  const total = useMemo(() => {
    let result = 0;
    foodList.forEach((e) => {
      if (e.discount_price) {
        result += (e.amount as number) * e.discount_price;
      } else {
        result += (e.amount as number) * (e.price as number);
      }
    });
    return result;
  }, [foodList]);

  const discountEach = useMemo(() => {
    const count = foodList.reduce(
      (prev, next) => prev + (next.amount as number),
      0,
    );
    const special = foodList.find((e) => e.discount_price);

    let addedDiscount = 0;
    if (special && special.discount_price) {
      addedDiscount = (special.price as number) - special.discount_price;
    }

    return Math.round((discount - shipFee + addedDiscount) / count);
  }, [foodList, shipFee, discount]);
  //#endregion

  //#region Event
  const onSaveNewItem = (newFood: IFood) => {
    setFoodList((prev) => [...prev, newFood]);
  };

  const onRemoveFood = (index: number) => () => {
    const result = foodList.filter((_e, i) => i !== index);
    setFoodList(result);
  };

  const onRenew = () => {
    setFoodList([]);
    setShipFee(0);
    setDiscount(0);
  };
  //#endregion

  //#region Render
  return (
    <Theme appearance="inherit">
      <Dialog.Root onOpenChange={() => orderRef.current?.clear()}>
        <Flex
          gap="3"
          align="center"
          justify="center"
          position="relative"
          style={{
            height: '100vh',
            border: '10px solid transparent',
            borderImage: `url("/christmas.jpg") 50 round`,
            borderImageWidth: 3,
          }}
        >
          <Card variant="classic" style={{ minWidth: '400px' }}>
            <Flex direction="column" gap="2" width="100%">
              <Flex gap="2">
                <Dialog.Trigger style={{ flex: 1 }}>
                  <Button variant="outline" mb="3">
                    <PlusIcon />
                    Thêm món
                  </Button>
                </Dialog.Trigger>
                <Button
                  variant="outline"
                  color="crimson"
                  onClick={onRenew}
                  style={{ flex: 1 }}
                >
                  Tạo đơn mới
                </Button>
              </Flex>

              <Text weight="medium">
                Danh sách món {!(foodList.length > 0) && 'đang trống'}
              </Text>
              {foodList.map((food, i) => (
                <Flex key={food.uid} align="center" justify="between">
                  <Flex direction="row" align="baseline">
                    <IconButton
                      color="red"
                      variant="surface"
                      onClick={onRemoveFood(i)}
                      style={{
                        cursor: 'pointer',
                        marginRight: '5px',
                        height: 18,
                        width: 18,
                      }}
                    >
                      <Cross2Icon width="12" height="12" />
                    </IconButton>
                    <Text style={{ lineHeight: '20px' }}>
                      {food.name}&nbsp;
                      <span
                        style={{ fontWeight: 500, fontSize: 14 }}
                      >{`(x${food.amount})`}</span>
                      <br />
                      <Text as="span" size="1">
                        {`Đơn giá ${formatMoney(food.price as number)}`}
                      </Text>
                    </Text>
                  </Flex>

                  {food.discount_price ? (
                    <Text
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        gap: 2,
                      }}
                    >
                      <img
                        src="/star.png"
                        alt=""
                        style={{ maxHeight: '20px' }}
                        className="anim-beat"
                      />
                      {formatMoney(
                        (food.discount_price as number) *
                          (food.amount as number),
                      )}
                    </Text>
                  ) : (
                    <Text>
                      {formatMoney(
                        (food.price as number) * (food.amount as number),
                      )}
                    </Text>
                  )}
                </Flex>
              ))}
              <Separator orientation="horizontal" size="4" />
              <Flex align="center" justify="between">
                <Text weight="medium">Tổng cộng</Text>
                <Text weight="medium"> {formatMoney(total)}</Text>
              </Flex>
              <Flex align="center" justify="between">
                <Text weight="medium">Phí giao hàng</Text>
                <Text weight="medium">{formatMoney(shipFee)}</Text>
              </Flex>
              <Flex align="center" justify="between">
                <Text weight="medium">Tiền giảm giá</Text>
                <Text weight="medium">{formatMoney(discount)}</Text>
              </Flex>
              <Flex align="center" justify="between">
                <Text weight="medium">Thành tiền</Text>
                <Text weight="medium">
                  {formatMoney(total + shipFee - discount)}
                </Text>
              </Flex>
            </Flex>
          </Card>
          <Flex direction="column" gap="3">
            <Card>
              <Flex direction="column" gap="2" style={{ minWidth: '400px' }}>
                <label>
                  <Text as="div" size="2" mb="1" weight="medium">
                    Phí giao hàng
                  </Text>
                  <TextField.Input
                    type="number"
                    value={shipFee}
                    onChange={(e) => setShipFee(Number(e.target.value))}
                    onClick={onSelectAll}
                    placeholder="Nhập phí giao hàng"
                  />
                </label>
                <label>
                  <Text as="div" size="2" mb="1" weight="medium">
                    Tiền giảm giá
                  </Text>
                  <TextField.Input
                    type="number"
                    value={discount}
                    onChange={(e) => setDiscount(Number(e.target.value))}
                    onClick={onSelectAll}
                    placeholder="Nhập tiền giảm giá"
                  />
                </label>
              </Flex>
            </Card>
            {foodList.length > 0 && (
              <Card>
                <Box style={{ minWidth: '400px' }}>
                  <Flex direction="column" gap="2">
                    <Flex align="center" justify="between">
                      <Text weight="medium">Tiền giảm mỗi phần</Text>
                      <Text weight="medium">
                        {formatMoney(discountEach || 0)}
                      </Text>
                    </Flex>
                    <Separator orientation="horizontal" size="4" />
                    {foodList.map((food) => (
                      <Flex key={food.uid} align="center" justify="between">
                        <Text>
                          {food.name}&nbsp;
                          <span
                            style={{ fontWeight: 500, fontSize: 14 }}
                          >{`(x${food.amount})`}</span>
                        </Text>
                        <Text>
                          {food.amount !== 1 && (
                            <Text
                              as="span"
                              weight="medium"
                              color="cyan"
                            >{`(${formatMoney(
                              (food.price as number) - discountEach,
                            )}) `}</Text>
                          )}
                          {formatMoney(
                            ((food.price as number) - discountEach) *
                              (food.amount as number),
                          )}
                        </Text>
                      </Flex>
                    ))}
                  </Flex>
                </Box>
              </Card>
            )}
          </Flex>
          <SwitchThemeButton />
        </Flex>
        <ModalContent ref={orderRef} onSave={onSaveNewItem} />
      </Dialog.Root>
    </Theme>
  );
  //#endregion
}

export default App;
